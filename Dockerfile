FROM node:11-alpine

WORKDIR /usr/src/app

RUN apk update && apk upgrade && apk add --no-cache bash git openssh
RUN curl -L https://unpkg.com/@pnpm/self-installer | node
RUN pnpm install -g pnpm
RUN pnpm install -g @microsoft/rush

CMD [ "node" ]